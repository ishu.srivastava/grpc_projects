package Day8;

import java.time.ZonedDateTime;
import java.time.ZoneId;

public class TimerPrint {

    public static void main(String[] args) throws InterruptedException {


        //loop for 10 times for 20 seconds every 2seconds
        for(int i = 0;i < 10 ; i++){
            System.out.println(ZonedDateTime.now(ZoneId.of("Asia/Kolkata")).getHour()+":"+ZonedDateTime.now(ZoneId.of("Asia/Kolkata")).getMinute()+":"+ZonedDateTime.now(ZoneId.of("Asia/Kolkata")).getSecond());
            Thread.sleep(2000);
        }
    }
}
