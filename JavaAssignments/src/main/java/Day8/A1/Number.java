package Day8.A1;

public class Number implements Runnable {

    private int tableNumber;

    public Number(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    @Override
    public void run() {

        for(int i = 1;i <= 10;i++){
            System.out.println("Thread for number:"+tableNumber);
            System.out.println(tableNumber+" * "+i+" = "+(this.tableNumber*i));
        }



    }
}
