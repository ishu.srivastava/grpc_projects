package Day8.A1;

public class RunnablePlaygroun {

    public static void main(String[] args) {

        //declaring thread
        Number thread1 = new Number(2);
        Number thread2 = new Number(5);
        Number thread3 = new Number(8);

        System.out.println("calling threads from main function...");


        Thread firstThread = new Thread(thread1);
        Thread secondThread = new Thread(thread2);
        Thread thirdThread = new Thread(thread3);

        firstThread.start();
        secondThread.start();
        thirdThread.start();

        //threads will run randomly and print data
        System.out.println("Threading is over");
    }
}
