package Day5;

public class Cat extends Animal implements Pet{

    private String name;

    public Cat(int legs, String name) {
        super(legs);
        this.name = name;
    }

    public Cat(int legs) {
        super(legs);
    }

    @Override
    public void walk() {
        System.out.println("Cat is walking");
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void eat() {
        System.out.println("Cat is eating");
    }


}
