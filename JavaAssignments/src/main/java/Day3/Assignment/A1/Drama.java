package Day3.Assignment.A1;

public class Drama extends Movie{
    //setting the default latefee

    private double totalLateFee;

    public Drama(String mpaaRating, int id, String title){
        super(mpaaRating,id,title);
    }


    private final double LATEFEECHARGES = 2.50;

    public double calcLateFee(int days){
        this.totalLateFee = LATEFEECHARGES*days;
        return this.totalLateFee;
    }
}
