package Day3.Assignment.A1;

public class Comedy extends Movie{


    //setting the default latefee

    private double totalLateFee=0.00;

    public Comedy(String mpaaRating, int id, String title){
        super(mpaaRating,id,title);
    }

    public double getTotalLateFee() {
        return totalLateFee;
    }

    public void setTotalLateFee(double totalLateFee) {
        this.totalLateFee = totalLateFee;
    }

    private final double LATEFEECHARGES = 2.00;

    public double calcLateFee(int days){
        this.totalLateFee = LATEFEECHARGES*days;
        return this.totalLateFee;
    }


}
