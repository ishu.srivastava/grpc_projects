package Day3.Assignment.A4;

public class Manager extends Employee{


    private String department;
    private int noOfReportees;

    public Manager(int empNo, String empName, String address, String contactNo, String department, int noOfReportees) {
        super(empNo, empName, address, contactNo);
        this.department = department;
        this.noOfReportees = noOfReportees;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public int getNoOfReportees() {
        return noOfReportees;
    }

    public void setNoOfReportees(int noOfReportees) {
        this.noOfReportees = noOfReportees;
    }


    @Override
    public String toString() {
        //fetching details from superclass
        StringBuilder sb = new StringBuilder(super.toString());

        //removing end parenthesis of super return
        sb.deleteCharAt(sb.length()-1);
        sb.append("Department:"+this.department+"\n");
        sb.append("No of Reportee:"+this.noOfReportees+"\n}");
        return sb.toString();
    }
}
