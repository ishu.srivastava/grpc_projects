package Day3.Assignment.A3;

public class AccountDetails extends AccountHolder{

    private String accountType;
    private double bal;

    public AccountDetails(long accountNumber, String name, String mobileNumber, String accountType, double bal) {
        super(accountNumber, name, mobileNumber);
        this.accountType = accountType;
        this.bal = bal;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public double getBal() {
        return bal;
    }

    public void setBal(double bal) {
        this.bal = bal;
    }


    @Override
    public String toString() {
        //appending details
        StringBuilder sb = new StringBuilder("{\n");
        sb.append("Name:"+this.getName()+"\n");
        sb.append("Mobile Number:"+this.getMobileNumber()+"\n");
        sb.append("Account Type:"+this.accountType+"\n");
        sb.append("Account Number"+this.getAccountNumber()+"\n");
        sb.append("Balance:"+this.getBal()+"\n}");

        //converting stringbuilder to string type
        return sb.toString();
    }
}
