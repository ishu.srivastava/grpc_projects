package Day3.Assignment.A3;

public class AccountPlayground {
    public static void main(String[] args) {
        AccountDetails a1 = new AccountDetails(781263612,"James","782738192381",AccountTypeEnum.SB.name(),7817.00);
        AccountDetails a2 = new AccountDetails(912378,"Jamison","17263761",AccountTypeEnum.FD.name(), 182192.00);

        System.out.println(a1.toString());
        System.out.println(a2.toString());
    }
}
