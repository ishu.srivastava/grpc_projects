package Day3.Assignment.A2;

public class Magazine extends Book{


    private String type;


    //constructor for populating data
    public Magazine(String isbn, String title, double price, String type) {
        super(isbn, title, price);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void printDetails(){
        StringBuilder sb = new StringBuilder("{\n");
        sb.append("ISBN:"+this.getIsbn()+"\n");
        sb.append("Title:"+this.getTitle()+"\n");
        sb.append("Price:"+this.getPrice()+"\n");
        sb.append("Type:"+this.type+"\n}");

        System.out.println(sb);

    }
}
