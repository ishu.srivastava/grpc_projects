package Day3.Assignment.A2;

public class Novel extends Book{


        private String author;


        //constructor for populating data
        public Novel(String isbn, String title, double price, String author) {
            super(isbn, title, price);
            this.author = author;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

    public void printDetails(){
        StringBuilder sb = new StringBuilder("{\n");
        sb.append("ISBN:"+this.getIsbn()+"\n");
        sb.append("Title:"+this.getTitle()+"\n");
        sb.append("Price:"+this.getPrice()+"\n");
        sb.append("Author:"+this.author+"\n}");

        System.out.println(sb);

    }
    }


