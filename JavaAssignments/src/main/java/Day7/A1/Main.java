package Day7.A1;

import java.lang.annotation.Annotation;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.DriverManager;

public class Main {
    public static void main(String[] args) {
        //creating class that implements annotation
        AnnotationUse a = new AnnotationUse();
        Class c = a.getClass();
        Annotation annotation = c.getAnnotation(DBParams.class);
        DBParams dbDetails = (DBParams) annotation;

        try{
            //connection establishing with database
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","postgres");
            Statement stmt = conn.createStatement();

            System.out.println("Connected to database:"+dbDetails.dbName()+" with user:"+dbDetails.userId()+" and pasword "+dbDetails.password());



        }catch (SQLException s){
            System.out.println(s);
            System.out.println("Unable to establish connection with database or invalid query");
        }






    }
}
