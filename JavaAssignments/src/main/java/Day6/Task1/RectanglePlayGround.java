package Day6.Task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RectanglePlayGround {

    public static void main(String[] args) {

        //creating rectangle object
        Rectangle rectangle = new Rectangle();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


        //handling I/O exception
        try{
            int length = Integer.parseInt(br.readLine());
            int breadth = Integer.parseInt(br.readLine());

            rectangle.setLength(length);
            rectangle.setBreadth(breadth);

            System.out.println("Area of rectangle is:"+rectangle.area());
            System.out.println("Perimeter of rectangle is:"+rectangle.perimeter());
        }catch (IOException e){
            System.out.println("I/O error");
        }catch (NumberFormatException e){
            System.out.println("Invalid input");
        }

    }
}
