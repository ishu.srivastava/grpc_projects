package Day6.Task2;

import java.io.BufferedReader;
import java.util.Scanner;

public class Division {

    private int x;
    private int y;

    public Division(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int divide() throws InvalidDivisor {

        if(y==0){
            throw new InvalidDivisor("The Division operation cannot be done as the divisor is 0");
        }else{
            int res = x/y;
            return res;
        }
    }
}
