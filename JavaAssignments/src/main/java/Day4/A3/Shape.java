package Day4.A3;

public abstract class Shape {
    protected int xCoordinate;
    protected int yCoordinate;


    public Shape(int x, int y){
        this.xCoordinate = x;
        this.yCoordinate= y;
    }

    //using method overloading for move implementation
    //negative for move back and positive for move ahead

    public void moveShape(int coordinate,char axis){
        if(axis == 'X' || axis == 'x'){
            this.xCoordinate = this.xCoordinate + coordinate;
            return;
        }
        this.yCoordinate = this.yCoordinate + coordinate;
    }

    //if we want to move shape on both axis
    public void moveShape(int xCoordinate,int yCoordinate){
        this.xCoordinate= this.xCoordinate + xCoordinate;
        this.yCoordinate = this.yCoordinate + yCoordinate;
    }

    @Override
    public String toString() {
        return this.xCoordinate +" , "+this.yCoordinate;
    }

    public abstract void show();
}
