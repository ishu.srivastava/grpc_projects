package Day4.A3;

public class Rectangle extends Shape{

        private int firstDiagonal;
        private int secondDiagonal;

        public Rectangle(int x, int y, int firstDiagonal, int secondDiagonal) {
                super(x, y);
                this.firstDiagonal = firstDiagonal;
                this.secondDiagonal = secondDiagonal;
        }

        public int getFirstDiagonal() {
                return firstDiagonal;
        }

        public void setFirstDiagonal(int firstDiagonal) {
                this.firstDiagonal = firstDiagonal;
        }

        public int getSecondDiagonal() {
                return secondDiagonal;
        }

        public void setSecondDiagonal(int secondDiagonal) {
                this.secondDiagonal = secondDiagonal;
        }

        @Override
        public String toString() {
                return this.firstDiagonal +" , "+this.secondDiagonal;
        }

        @Override
        public void show() {
                System.out.println("Showing Rectangle at location ("+super.toString()+") with diagonals at ("+this.toString()+")");
        }
}
