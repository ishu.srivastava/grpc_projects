package Day4.A3;

public class Circle extends Shape{

    private float radius;

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public Circle(int x, int y, float rad) {
        super(x, y);
        this.radius = rad;
    }

    @Override
    public String toString() {
        return String.valueOf(this.radius);
    }

    @Override
    public void show() {
        System.out.println("Showing Circle at location ("+super.toString()+") with radius "+this.toString());
    }
}
