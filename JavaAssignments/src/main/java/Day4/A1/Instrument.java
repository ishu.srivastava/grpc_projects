package Day4.A1;

public abstract class Instrument {

    public abstract String play();
}
