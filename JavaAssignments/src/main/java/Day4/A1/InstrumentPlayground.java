package Day4.A1;

import java.util.ArrayList;
import java.util.List;

public class InstrumentPlayground {
    public static void main(String[] args) {
        List<Instrument> listOfInstrument = new ArrayList<>();
        //adding 10 random instrument to array
        listOfInstrument.add(new Flute());
        listOfInstrument.add(new Piano());
        listOfInstrument.add(new Guitar());
        listOfInstrument.add(new Piano());
        listOfInstrument.add(new Guitar());

        listOfInstrument.add(new Flute());
        listOfInstrument.add(new Piano());
        listOfInstrument.add(new Guitar());
        listOfInstrument.add(new Piano());
        listOfInstrument.add(new Guitar());


        for (Instrument i:listOfInstrument
             ) {

            //checking what instance each object is
            if(i instanceof Flute){
                System.out.print("This is a Flute Object:");
            } else if (i instanceof Guitar) {
                System.out.print("This is a Guitar Object:");

            }else {
                System.out.print("This is a Piano Object:");
            }

            //implementing play function which is overridden
            System.out.println(i.play());
        }
    }





}
