package Day4.A2;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class TestCompartment {

    public static void main(String[] args) {


        //creating a list of compartments
        List<Compartment> compartments = new ArrayList<>();

        ///importing random and generating random integer to add  compartment

        Random random = new Random();

        for(int i = 0;i < 10; i++){

            //switch case to add compartments based on random integer generated
            int compartment = random.nextInt(4);
            switch (compartment){
                case 0:
                    compartments.add(new FirstClass());
                    break;
                case 1:
                    compartments.add(new Ladies());
                    break;
                case 2:
                    compartments.add(new General());
                    break;
                case 3:
                    compartments.add(new Luggage());
                    break;
            }

        }

        //showing notice function for each compartment generated

        for (Compartment c:compartments
             ) {
            System.out.println(c.notice());
        }
    }


}
