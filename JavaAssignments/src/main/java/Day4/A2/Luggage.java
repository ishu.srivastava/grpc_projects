package Day4.A2;

public class Luggage extends Compartment{

    @Override
    public String notice() {
        return "This is a Luggage Compartment";
    }
}
