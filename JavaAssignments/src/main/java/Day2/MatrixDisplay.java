package Day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MatrixDisplay {
    public static void main(String[] args) {

        //taking the size of matrix
        System.out.println("Enter Matrix Size");
        System.out.print("No. of Row:");
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt();
        System.out.print("No. of Columns:");
        int column = sc.nextInt();

        //insert value in matrix
        System.out.println("Enter the values:");
        List<ArrayList<Integer>> matrixList = new ArrayList<>();

        for(int i = 0 ; i < row;i++){
            ArrayList<Integer> tempList = new ArrayList<>();
            for(int j = 0;j < column;j++){
                System.out.print("Enter value for "+(i+1)+" row "+(j+1)+" column:");
                tempList.add(sc.nextInt());
            }
            matrixList.add(tempList);
        }


            displayMatrix(matrixList);




    }


    //display list function
    public static void displayMatrix(List<ArrayList<Integer>> matrixList){
        for (List<Integer> list:matrixList
             ) {
            for (int l:list
                 ) {
                System.out.print(l+ "\t");

            }
            System.out.println();

        }
    }

}
