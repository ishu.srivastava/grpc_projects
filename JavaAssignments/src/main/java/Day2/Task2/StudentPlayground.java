package Day2.Task2;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StudentPlayground {


    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();

        //adding 5 students
        studentList.add(new Student(1,"Nikhil","12939218","BTECH",1234.75));
        studentList.add(new Student(2,"Richa","17328192","BCA",131.75));
        studentList.add(new Student(3,"Arvind","8912739","BBA",9871.54));
        studentList.add(new Student(4,"Pankaj","1920831","BSC",239.98));
        studentList.add(new Student(5,"Preetam","1892901","ARTS",1929.90));

        //sorting student on Course
        Collections.sort(studentList);

        double totalFee = 0;
        //getting total fee and printing details
        for (Student st:studentList
             ) {
            totalFee = totalFee+st.getFees();
            st.displayStudentDetails();
        }

        //formatting upto 2 decimal places
        DecimalFormat df = new DecimalFormat("0.00");
        System.out.println("Total fee of all students:"+df.format(totalFee));

    }
}
