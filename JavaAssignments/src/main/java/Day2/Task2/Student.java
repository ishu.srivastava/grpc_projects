package Day2.Task2;

public class Student implements Comparable<Student>{
    private int  studentId;
    private String studentName;
    private String mobileNumber;
    private String  course;
    private double fees = 0;


    //constructor to populate
    public Student(int id, String name, String mobileNumber,String course,double fees){
        this.studentId = id;
        this.studentName = name;
        this.mobileNumber = mobileNumber;
        this.course = course;
        this.fees = fees;
    }

    //function to display details
    public void displayStudentDetails(){
        System.out.println("{");
        System.out.println("ID:"+this.studentId);
        System.out.println("Name:"+this.studentName);
        System.out.println("Contact Number:"+this.mobileNumber);
        System.out.println("Course:"+this.course);
        System.out.println("Fees:"+this.fees);
        System.out.println("}");
    }

   //getter for course fee
    public double getFees() {
        return fees;
    }

    public int getStudentId() {
        return studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getCourse() {
        return course;
    }

    @Override
    public int compareTo(Student s) {
        if(this.course.charAt(0) > s.course.charAt(0)){
            return 1;
        } else if (this.course.charAt(0) < s.course.charAt(0)) {
            return -1;
        }else{
            int i = 1;
            while(this.course.charAt(i) == s.course.charAt(i)){
                i++;
            }
            if(i > s.studentName.length()){
                return 0;
            }
            if(this.course.charAt(0) > s.course.charAt(0)) {
                return 1;
            }else{
                return -1;
            }
        }
    }
}



