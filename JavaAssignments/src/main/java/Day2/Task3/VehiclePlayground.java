package Day2.Task3;

public class VehiclePlayground {
    public static void main(String[] args) {
        //creating 2 vehile class
        Vehicle motor1 = new Vehicle(2988,"Ferrari",30.99,3900129.90);
        Vehicle motor2 = new Vehicle(7903,"Mercedes",36.58,2381298.89);

        //displaying motor having lower price

        if(motor1.getPrice() < motor2.getPrice()){
            motor1.displayVehicleDetails();
        }else{
            motor2.displayVehicleDetails();
        }

    System.out.println("-----------------------------");
        //displaying vehicle with best mileage
        if(motor1.getMileage() > motor2.getMileage()){
            motor1.displayVehicleDetails();

        }else {
            motor2.displayVehicleDetails();
        }
    }
}
