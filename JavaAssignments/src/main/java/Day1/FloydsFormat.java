package Day1;

public class FloydsFormat {

    public static void floydPattern(int number){


        int i = 1;

        //looping until input number
        while(i < number){

            //loop to print pattern
           for(int j = 0;j < i;j++){
               System.out.print("*");
           }
           System.out.println();
           i++;
        }
    }

    public static void main(String[] args) {
        floydPattern(10);
    }
}
