package Day1;

public class Factorial {


    //calculate factorial function
    public static int factorial(int number){

        //default cases
        if(number < 0){
            System.out.println("Invalid Number");
            return Integer.MIN_VALUE;
        }
        if(number == 1){
            return 1;
        }


        int num = 1;
        int i = number;


        //Loop until factorial calculation
        do{
            num = num * i;
            i--;
        }while(i >= 1);

        return num;
    }
}
