import Day7.A6.Vectors;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotEquals;



public class Vectorstest {

    @Test
    public void vectorEqualTest(){

        System.out.println("Starting test for vector equal method...");

        int[] a = {2,3,5,1,9};
        int[] b = {2,3,5,1,9};
        assertTrue(Vectors.equal(a,b));

        System.out.println("positive test passes...");

        int[] a2= {1,3,5,6,2};
        int[] b2 = {3,12,4,5,2};
        assertFalse(Vectors.equal(a2,b2));

        System.out.println("Equal method test passed");


    }


    @Test
    public void scalerMultiplicationTest(){
        System.out.println("Starting test for vector scalermultiplication method...");

        int[] a = {2,3,4,5,6};
        int[] b = {1,3,4,5,3};



        assertEquals(70,Vectors.scalarMultiplication(a,b));
        System.out.println("positive test passes...");
        assertNotEquals(90,Vectors.scalarMultiplication(a,b));

        System.out.println("Scaler Multiplication method test passed");

    }


}
