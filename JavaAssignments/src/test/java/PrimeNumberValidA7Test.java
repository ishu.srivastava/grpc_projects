import Day7.PrimeNumberValidator;
import org.junit.Test;

import static org.junit.Assert.*;

public class PrimeNumberValidA7Test {

@Test
    public void validatorTest(){
    PrimeNumberValidator pv = new PrimeNumberValidator();
    assertFalse(pv.validate(12));
    assertTrue(pv.validate(13));
}
}
