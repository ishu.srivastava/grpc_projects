import Day2.Account;
import Day2.Task2.Student;
import Day2.Task3.Vehicle;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day2Test {


    //student Constructor test

    @Test
    public void studentConstructorCheck(){
        Student st = new Student(2,"Nikhil","89123","BTECH",8319.00);
        assertEquals(2,st.getStudentId());
        assertEquals("Nikhil",st.getStudentName());
        assertEquals("89123",st.getMobileNumber());
        assertEquals("BTECH",st.getCourse());
        assertEquals(8319.00,st.getFees(),0.3);
    }

    //Vehicle Constructor test

    @Test
    public void vehicleConstructorCheck(){
        Vehicle vh = new Vehicle(3232,"Ferrari",89.19,8319.00);
        assertEquals(3232,vh.getRegNo());
        assertEquals("Ferrari",vh.getBrand());
        assertEquals(89.19,vh.getMileage(),0);
        assertEquals(8319.00,vh.getPrice(),0);

    }


    //Account class Test

    @Test
    public void accountConstructorTest(){
        Account ac = new Account(14126.97);
        assertEquals(14126.97,ac.getBalance(),0);
    }


    //testing deposit and withdraw function

    @Test
    public void depoositAndWithdrawTest(){
        Account ac = new Account(2345.78);
        ac.deposit(1200.22);
        assertEquals(3546.00,ac.getBalance(),0.1);
        ac.withdraw(1200.22);
        assertEquals(2345.78,ac.getBalance(),0.1);
    }




}
