package com.gl.grpcAssign;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: Token.proto")
public final class tokenGrpc {

  private tokenGrpc() {}

  public static final String SERVICE_NAME = "token";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.gl.grpcAssign.Token.tokenMessage,
      com.gl.grpcAssign.Token.APIResponse> getGettokenMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "gettoken",
      requestType = com.gl.grpcAssign.Token.tokenMessage.class,
      responseType = com.gl.grpcAssign.Token.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.gl.grpcAssign.Token.tokenMessage,
      com.gl.grpcAssign.Token.APIResponse> getGettokenMethod() {
    io.grpc.MethodDescriptor<com.gl.grpcAssign.Token.tokenMessage, com.gl.grpcAssign.Token.APIResponse> getGettokenMethod;
    if ((getGettokenMethod = tokenGrpc.getGettokenMethod) == null) {
      synchronized (tokenGrpc.class) {
        if ((getGettokenMethod = tokenGrpc.getGettokenMethod) == null) {
          tokenGrpc.getGettokenMethod = getGettokenMethod = 
              io.grpc.MethodDescriptor.<com.gl.grpcAssign.Token.tokenMessage, com.gl.grpcAssign.Token.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "token", "gettoken"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.gl.grpcAssign.Token.tokenMessage.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.gl.grpcAssign.Token.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new tokenMethodDescriptorSupplier("gettoken"))
                  .build();
          }
        }
     }
     return getGettokenMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static tokenStub newStub(io.grpc.Channel channel) {
    return new tokenStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static tokenBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new tokenBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static tokenFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new tokenFutureStub(channel);
  }

  /**
   */
  public static abstract class tokenImplBase implements io.grpc.BindableService {

    /**
     */
    public void gettoken(com.gl.grpcAssign.Token.tokenMessage request,
        io.grpc.stub.StreamObserver<com.gl.grpcAssign.Token.APIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGettokenMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGettokenMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.gl.grpcAssign.Token.tokenMessage,
                com.gl.grpcAssign.Token.APIResponse>(
                  this, METHODID_GETTOKEN)))
          .build();
    }
  }

  /**
   */
  public static final class tokenStub extends io.grpc.stub.AbstractStub<tokenStub> {
    private tokenStub(io.grpc.Channel channel) {
      super(channel);
    }

    private tokenStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected tokenStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new tokenStub(channel, callOptions);
    }

    /**
     */
    public void gettoken(com.gl.grpcAssign.Token.tokenMessage request,
        io.grpc.stub.StreamObserver<com.gl.grpcAssign.Token.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGettokenMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class tokenBlockingStub extends io.grpc.stub.AbstractStub<tokenBlockingStub> {
    private tokenBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private tokenBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected tokenBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new tokenBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.gl.grpcAssign.Token.APIResponse gettoken(com.gl.grpcAssign.Token.tokenMessage request) {
      return blockingUnaryCall(
          getChannel(), getGettokenMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class tokenFutureStub extends io.grpc.stub.AbstractStub<tokenFutureStub> {
    private tokenFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private tokenFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected tokenFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new tokenFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.gl.grpcAssign.Token.APIResponse> gettoken(
        com.gl.grpcAssign.Token.tokenMessage request) {
      return futureUnaryCall(
          getChannel().newCall(getGettokenMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GETTOKEN = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final tokenImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(tokenImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GETTOKEN:
          serviceImpl.gettoken((com.gl.grpcAssign.Token.tokenMessage) request,
              (io.grpc.stub.StreamObserver<com.gl.grpcAssign.Token.APIResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class tokenBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    tokenBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.gl.grpcAssign.Token.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("token");
    }
  }

  private static final class tokenFileDescriptorSupplier
      extends tokenBaseDescriptorSupplier {
    tokenFileDescriptorSupplier() {}
  }

  private static final class tokenMethodDescriptorSupplier
      extends tokenBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    tokenMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (tokenGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new tokenFileDescriptorSupplier())
              .addMethod(getGettokenMethod())
              .build();
        }
      }
    }
    return result;
  }
}
