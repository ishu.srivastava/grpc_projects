package com.gl.grpcAssign.service;

import com.gl.grpcAssign.Token;
import com.gl.grpcAssign.User;
import com.gl.grpcAssign.tokenGrpc;
import com.gl.grpcAssign.userGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

public class TokenService extends tokenGrpc.tokenImplBase{

    @Override
    public void gettoken(Token.tokenMessage request, StreamObserver<Token.APIResponse> responseObserver) {

        ManagedChannel channel= ManagedChannelBuilder.forAddress(
                "localhost",8085).usePlaintext().build();

        User.loginMessage request1 = User.loginMessage.newBuilder().setUsername(request.getUsername()).setPassword(request.getUsername()).build();

        userGrpc.userBlockingStub userStub = userGrpc.newBlockingStub(channel);

        User.APIResponse response = userStub.login(request1);
        Token.APIResponse.Builder responceToken = Token.APIResponse.newBuilder();

            if(response.getResponseMessage().equals("SUCCESS")){
                responceToken.setResponseMessage("Token 123");
            }
            else{
                responceToken.setResponseMessage("Failure 0");
            }

            responseObserver.onNext(responceToken.build());
            responseObserver.onCompleted();

    }
}
