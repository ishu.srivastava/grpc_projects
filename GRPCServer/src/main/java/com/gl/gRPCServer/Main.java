package com.gl.gRPCServer;

import com.gl.gRPCServer.service.UserService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {
        System.out.println("Starting User Server");
        Server server=ServerBuilder.forPort(8085).addService(new UserService()).build();

            server.start();
            System.out.println("Server started on port number: "+server.getPort());
            server.awaitTermination();


    }
}