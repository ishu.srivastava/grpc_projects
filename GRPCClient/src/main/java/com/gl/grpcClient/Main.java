package com.gl.grpcClient;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class Main {
    public static void main(String[] args) {
        System.out.println("Creating a channel between client and server");

        ManagedChannel channel= ManagedChannelBuilder.forAddress(
                "localhost",8090).usePlaintext().build();

        User.loginMessage request = User.loginMessage.newBuilder().setUsername("ishu").setPassword("ishu").build();

        userGrpc.userBlockingStub userStub = userGrpc.newBlockingStub(channel);

        User.APIResponse response = userStub.login(request);

        System.out.println("Responce from Server"+response.getResponseMessage());

    }
}
