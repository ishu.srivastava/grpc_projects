package com.grpc.amazon.signIn;

import com.grpc.amazon.login.AmazoneDB;
import io.grpc.stub.StreamObserver;

import java.sql.SQLException;

public class CreateAccount extends createProfileGrpc.createProfileImplBase{
    @Override
    public void userProfile(CreateProfile.UserAccount request, StreamObserver<CreateProfile.APIResponse> responseObserver) throws SQLException {
        System.out.println("Create User Profile");
        CreateProfile.APIResponse.Builder responce = CreateProfile.APIResponse.newBuilder();

            try{

            new AmazoneDB().craeteUserAccount(request);

            if (request.getName().equals("")) {
                responce.setResponseMessage("Please Enter your name");
                responce.setResponseCode(100);
            } else if (request.getMobileNo() == 0) {
                responce.setResponseMessage("Please Enter your contact number");
                responce.setResponseCode(100);
            } else if (request.getEmail().equals("")) {
                responce.setResponseMessage("please Enter your Email");
                responce.setResponseCode(100);
            } else if (request.getPassword().equals("")) {
                responce.setResponseMessage("Please Enter your password");
                responce.setResponseCode(100);
            } else if (request.getAddress().equals("")) {
                responce.setResponseMessage("Please Enter your address");
                responce.setResponseCode(100);
            } else if (request.getState().equals("")) {
                responce.setResponseMessage("please Enter your state");
                responce.setResponseCode(100);
            } else if (request.getPincode() == 0) {
                responce.setResponseMessage("Please Enter your pincode");
                responce.setResponseCode(100);
            } else if (request.getCountry().equals("")) {
                responce.setResponseMessage("Please Enter your Country");
                responce.setResponseCode(100);
            } else {
                responce.setResponseMessage("Your Profile is created Succesfully");
                responce.setResponseCode(200);
            }
            responseObserver.onNext(responce.build());
            responseObserver.onCompleted();

        }catch(Exception e){
            responce.setResponseMessage("DB Error");
            responce.setResponseCode(100);

            responseObserver.onNext(responce.build());
            responseObserver.onCompleted();
        }
    }

    @Override
    public void businessProfile(CreateProfile.BusinessAccount request, StreamObserver<CreateProfile.APIResponse> responseObserver) {
        System.out.println("Create Business Profile");
        CreateProfile.APIResponse.Builder responce=CreateProfile.APIResponse.newBuilder();

        try{

            new AmazoneDB().createBusinessAccount(request);

            if(request.getName().equals("")){
                responce.setResponseMessage("Please Enter your name");
                responce.setResponseCode(100);
            } else if (request.getContactNo()==0) {
                responce.setResponseMessage("Please Enter Your Contact Number");
                responce.setResponseCode(100);
            } else if (request.getBusinessEmail().equals("")){
                responce.setResponseMessage("Please Enter Your Email Id");
                responce.setResponseCode(100);
            } else if (request.getPassword().equals("")){
                responce.setResponseMessage("Please Enter Your Password");
                responce.setResponseCode(100);
            } else if (request.getBrandList().equals("")) {
                responce.setResponseMessage("Please Enter your Brand List");
                responce.setResponseCode(100);
            } else if(request.getDevicesList().equals("")){
                responce.setResponseMessage("Please Enter your Device List");
                responce.setResponseCode(100);
            } else if (request.getSpecification().equals("")) {
                responce.setResponseMessage("Please Enter the Specification");
                responce.setResponseCode(100);
            } else if (request.getPrice()==0) {
                responce.setResponseMessage("Please Enter the Device Price");
                responce.setResponseCode(100);
            } else if (request.getShopAddress().equals("")) {
                responce.setResponseMessage("please Enter the Shop Address");
                responce.setResponseCode(100);
            } else if (request.getCountry().equals("")) {
                responce.setResponseMessage("Please Enter you Country");
                responce.setResponseCode(100);
            }
            else{
                responce.setResponseMessage("Your Account is Created Successfully");
                responce.setResponseCode(200);
            }
            responseObserver.onNext(responce.build());
            responseObserver.onCompleted();


        }catch(Exception s){
            responce.setResponseMessage("DB Error");
            responce.setResponseCode(100);

            responseObserver.onNext(responce.build());
            responseObserver.onCompleted();

        }

    }
}
