package com.grpc.amazon.login;

import com.grpc.amazon.amazonGrpc;
import com.grpc.amazon.Amazon;
import io.grpc.stub.StreamObserver;

import java.sql.SQLException;

public class loginpage extends amazonGrpc.amazonImplBase{


    @Override
    public void login(Amazon.loginMessage request, StreamObserver<Amazon.APIResponse> responseObserver){
        System.out.println("Please Enter your credentials");
        Amazon.APIResponse.Builder responce= Amazon.APIResponse.newBuilder();

        try {

            String username = request.getUsername();
            String password = request.getPassword();

             new AmazoneDB().loginData(request,responce,responseObserver);

            if(username.equals("")) {
                responce.setResponseMessage("Please Enter your Username");
                responce.setResponseCode(100);
            }
            else if(password.equals("")){
                responce.setResponseMessage("Please Enter your Password");
                responce.setResponseCode(100);
            }
            responseObserver.onNext(responce.build());
            responseObserver.onCompleted();
        }
        catch (SQLException s){

            responce.setResponseMessage("DB Error");
            responce.setResponseCode(100);

            responseObserver.onNext(responce.build());
            responseObserver.onCompleted();
        }

    }

    @Override
    public void logout(Amazon.empty request, StreamObserver<Amazon.APIResponse> responseObserver) {
        super.logout(request, responseObserver);
    }
}
