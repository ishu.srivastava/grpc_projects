package com.gl.grpcAss;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: Token.proto")
public final class tokenGrpc {

  private tokenGrpc() {}

  public static final String SERVICE_NAME = "token";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.gl.grpcAss.Token.loginMessage,
      com.gl.grpcAss.Token.APIResponse> getLoginMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "login",
      requestType = com.gl.grpcAss.Token.loginMessage.class,
      responseType = com.gl.grpcAss.Token.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.gl.grpcAss.Token.loginMessage,
      com.gl.grpcAss.Token.APIResponse> getLoginMethod() {
    io.grpc.MethodDescriptor<com.gl.grpcAss.Token.loginMessage, com.gl.grpcAss.Token.APIResponse> getLoginMethod;
    if ((getLoginMethod = tokenGrpc.getLoginMethod) == null) {
      synchronized (tokenGrpc.class) {
        if ((getLoginMethod = tokenGrpc.getLoginMethod) == null) {
          tokenGrpc.getLoginMethod = getLoginMethod = 
              io.grpc.MethodDescriptor.<com.gl.grpcAss.Token.loginMessage, com.gl.grpcAss.Token.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "token", "login"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.gl.grpcAss.Token.loginMessage.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.gl.grpcAss.Token.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new tokenMethodDescriptorSupplier("login"))
                  .build();
          }
        }
     }
     return getLoginMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.gl.grpcAss.Token.empty,
      com.gl.grpcAss.Token.APIResponse> getLogoutMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "logout",
      requestType = com.gl.grpcAss.Token.empty.class,
      responseType = com.gl.grpcAss.Token.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.gl.grpcAss.Token.empty,
      com.gl.grpcAss.Token.APIResponse> getLogoutMethod() {
    io.grpc.MethodDescriptor<com.gl.grpcAss.Token.empty, com.gl.grpcAss.Token.APIResponse> getLogoutMethod;
    if ((getLogoutMethod = tokenGrpc.getLogoutMethod) == null) {
      synchronized (tokenGrpc.class) {
        if ((getLogoutMethod = tokenGrpc.getLogoutMethod) == null) {
          tokenGrpc.getLogoutMethod = getLogoutMethod = 
              io.grpc.MethodDescriptor.<com.gl.grpcAss.Token.empty, com.gl.grpcAss.Token.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "token", "logout"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.gl.grpcAss.Token.empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.gl.grpcAss.Token.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new tokenMethodDescriptorSupplier("logout"))
                  .build();
          }
        }
     }
     return getLogoutMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static tokenStub newStub(io.grpc.Channel channel) {
    return new tokenStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static tokenBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new tokenBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static tokenFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new tokenFutureStub(channel);
  }

  /**
   */
  public static abstract class tokenImplBase implements io.grpc.BindableService {

    /**
     */
    public void login(com.gl.grpcAss.Token.loginMessage request,
        io.grpc.stub.StreamObserver<com.gl.grpcAss.Token.APIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getLoginMethod(), responseObserver);
    }

    /**
     */
    public void logout(com.gl.grpcAss.Token.empty request,
        io.grpc.stub.StreamObserver<com.gl.grpcAss.Token.APIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getLogoutMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getLoginMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.gl.grpcAss.Token.loginMessage,
                com.gl.grpcAss.Token.APIResponse>(
                  this, METHODID_LOGIN)))
          .addMethod(
            getLogoutMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.gl.grpcAss.Token.empty,
                com.gl.grpcAss.Token.APIResponse>(
                  this, METHODID_LOGOUT)))
          .build();
    }
  }

  /**
   */
  public static final class tokenStub extends io.grpc.stub.AbstractStub<tokenStub> {
    private tokenStub(io.grpc.Channel channel) {
      super(channel);
    }

    private tokenStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected tokenStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new tokenStub(channel, callOptions);
    }

    /**
     */
    public void login(com.gl.grpcAss.Token.loginMessage request,
        io.grpc.stub.StreamObserver<com.gl.grpcAss.Token.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLoginMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void logout(com.gl.grpcAss.Token.empty request,
        io.grpc.stub.StreamObserver<com.gl.grpcAss.Token.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLogoutMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class tokenBlockingStub extends io.grpc.stub.AbstractStub<tokenBlockingStub> {
    private tokenBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private tokenBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected tokenBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new tokenBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.gl.grpcAss.Token.APIResponse login(com.gl.grpcAss.Token.loginMessage request) {
      return blockingUnaryCall(
          getChannel(), getLoginMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.gl.grpcAss.Token.APIResponse logout(com.gl.grpcAss.Token.empty request) {
      return blockingUnaryCall(
          getChannel(), getLogoutMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class tokenFutureStub extends io.grpc.stub.AbstractStub<tokenFutureStub> {
    private tokenFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private tokenFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected tokenFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new tokenFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.gl.grpcAss.Token.APIResponse> login(
        com.gl.grpcAss.Token.loginMessage request) {
      return futureUnaryCall(
          getChannel().newCall(getLoginMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.gl.grpcAss.Token.APIResponse> logout(
        com.gl.grpcAss.Token.empty request) {
      return futureUnaryCall(
          getChannel().newCall(getLogoutMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_LOGIN = 0;
  private static final int METHODID_LOGOUT = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final tokenImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(tokenImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_LOGIN:
          serviceImpl.login((com.gl.grpcAss.Token.loginMessage) request,
              (io.grpc.stub.StreamObserver<com.gl.grpcAss.Token.APIResponse>) responseObserver);
          break;
        case METHODID_LOGOUT:
          serviceImpl.logout((com.gl.grpcAss.Token.empty) request,
              (io.grpc.stub.StreamObserver<com.gl.grpcAss.Token.APIResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class tokenBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    tokenBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.gl.grpcAss.Token.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("token");
    }
  }

  private static final class tokenFileDescriptorSupplier
      extends tokenBaseDescriptorSupplier {
    tokenFileDescriptorSupplier() {}
  }

  private static final class tokenMethodDescriptorSupplier
      extends tokenBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    tokenMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (tokenGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new tokenFileDescriptorSupplier())
              .addMethod(getLoginMethod())
              .addMethod(getLogoutMethod())
              .build();
        }
      }
    }
    return result;
  }
}
