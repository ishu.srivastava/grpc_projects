package com.gl.grpcAss.client;

import com.gl.grpcAss.Token;
import com.gl.grpcAss.tokenGrpc;

public class ClinetService extends tokenGrpc.tokenImplBase{

    @Override
    public void login(Token.loginMessage request, io.grpc.stub.StreamObserver<Token.APIResponse> responseObserver) {
        System.out.println("Inside Login method");
        String username=request.getUsername();
        String password=request.getPassword();

        Token.APIResponse.Builder responce = Token.APIResponse.newBuilder();
        if(username.equals(password)){
            responce.setResponseMessage("SUCCESS");
            responce.setResponseCode(200);
        }
        else{
            responce.setResponseMessage("Inav;lid User Name and data");
            responce.setResponseCode(400);
        }
        responseObserver.onNext(responce.build());
        responseObserver.onCompleted();


    }

    @Override
    public void logout(Token.empty request, io.grpc.stub.StreamObserver<Token.APIResponse> responseObserver) {
        super.logout(request, responseObserver);
    }
}
